// Keep everything in anonymous function, called on window load.
//import ExifReader from './exifreader/src/exif-reader.js'; 

if(window.addEventListener) {
    let files = new Array();
    let fileNames = new Array();
    let resX = new Array(); 
    let resY = new Array(); 
    let frame = -1, scale = 1;
    let gridSize = 60, grid = false, gridColor = '#00FFE2';
    let sliderF, valueF, fileInput, clearFiles, colorSelect, sliderZoom, xResDisp, yResDisp, xDimDisp, yDimDisp, distanceDisp, angleDisp, gridSelect, gridColorSelect, gridSizeSelect; 
  /// NEW CODE FOR TABLE
  //create Tabulator on DOM element with id "example-table"
    var table = new Tabulator("#measurement-table", {
        height:false, // set height of table to enable virtual DOM
        maxHeight:800,
        layout:"fitColumns",
        selectablePersistence:false,
        columns:[ //Define Table Columns
            {formatter:"rowSelection", title:"select all", titleFormatter:"rowSelection", hozAlign:"center", headerSort:false, width:30},
            
            {title:"X", field:"posx", sorter:"number", hozAlign:"right", width:30},
            {title:"Y", field:"posy", sorter:"number", hozAlign:"right", width:30},
            {title:"Angle", field:"angle", sorter:"number", hozAlign:"right", width:80},
            {title:"Outcome", field:"outcome", sorter:"string", hozAlign:"right", editable:"true", editor:"select", editorParams:{
                values:["undefined", "catastrophe", "crossover", "zippering"], 
                defaultValue:"undefined", //set the value that should be selected by default if the cells value is undefined
        }},
        {title:"Image", field:"frame", sorter:"number", hozAlign:"right"},
        ],
    });
    /// NEW CODE END
    
    window.addEventListener('load', function () {
        var canvas, context;
        var imageCanvas, imageContext;
        var canvasWrapper;
        var color = '#A6FF00';
        
        // The active tool instance.
        var tool = false;
        var tool_default = 'rect';

        // Initialization sequence.
        function init () {
            // Find the wrapper 
            canvasWrapper = document.getElementById('canvasWrapper'); 
            if (!canvasWrapper) {
                alert('Error: I cannot find the canvas wrapper element!');
                return;
            }
            // Find the canvas elements.

            imageCanvas = document.getElementById('images');
            if (!imageCanvas) {
                alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!imageCanvas.getContext) {
                alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            imageContext = imageCanvas.getContext('2d');
            if (!imageContext) {
                alert('Error: failed to getContext!');
                return;
            }  
            canvas = document.getElementById('drawing');
            if (!canvas) {
                alert('Error: I cannot find the canvas element!');
                return;
            }

            if (!canvas.getContext) {
                alert('Error: no canvas.getContext!');
                return;
            }

            // Get the 2D canvas context.
            context = canvas.getContext('2d');
            if (!context) {
                alert('Error: failed to getContext!');
                return;
            }
            updateColor();

            /// GET THE I/O HTML OBJECTS
            fileInput = document.getElementById('get-files');
            fileInput.addEventListener("click", function(){this.value=null;}, false); 
            fileInput.addEventListener("input", handleFiles, false); 

            clearFiles = document.getElementById('clear');
            clearFiles.addEventListener("click", clearImages, false);
            colorSelect = document.getElementById('drawcolor'); 
            colorSelect.addEventListener("change", changeColor, false);
            sliderF = document.getElementById('sliderF');
            sliderF.addEventListener("input", function(e){setFrame(e.target.value);}, false);
            valueF = document.getElementById('valueF');
            sliderZoom = document.getElementById('sliderZoom');
            sliderZoom.addEventListener("input", function(e){setZoom(e.target.value);}, false); 
            gridSelect = document.getElementById('gridSelect');
            gridSelect.addEventListener("change", function(){if ( this.checked ) { grid = true; } else { grid = false; } displayImage(); }, false);
            gridColorSelect = document.getElementById('gridColor'); 
            gridColorSelect.addEventListener("change", function() { gridColor = this.value; displayImage();}, false);
            gridSizeSelect = document.getElementById('gridRes');
            gridSizeSelect.addEventListener("input", function(e){gridSize = parseFloat(e.target.value); var gridSizeValueDisplay = gridSize.toString().concat('px'); valueGridRes.innerHTML = gridSizeValueDisplay;  displayImage();}, false);  

            xResDisp = document.getElementById('xRes'); 
            xResDisp.addEventListener("input", function(e){var xRes = parseFloat(e.target.value); if (xRes == 'NaN') {alert("X-Resolution should be a number in \u03BCm!");} for (var i = 0; i < resX.length; i++) {resX[i] = xRes; xDimDisp.value = (resX[i]*files[i].width).toFixed(2).toString().concat(" \u03BCm");}}); 
            yResDisp = document.getElementById('yRes'); 
            yResDisp.addEventListener("input", function(e){var yRes = parseFloat(e.target.value); if (xRes == 'NaN') {alert("X-Resolution should be a number in \u03BCm!");} for (var i = 0; i < resY.length; i++) {resY[i] = yRes; yDimDisp.value = (resY[i]*files[i].height).toFixed(2).toString().concat(" \u03BCm");}}); 
            xDimDisp = document.getElementById('xDim'); 
            xDimDisp.addEventListener("input", function(e){var xDim = parseFloat(e.target.value); if (xDim == 'NaN') {alert("X-Dimension should be a number in \u03BCm!");} for (var i = 0; i < resX.length; i++) {resX[i] = xDim/files[i].width; xResDisp.value = resX[i].toFixed(2).toString().concat(" \u03BCm");}}); 

            yDimDisp = document.getElementById('yDim'); 
            yDimDisp.addEventListener("input", function(e){var yDim = parseFloat(e.target.value); if (yDim == 'NaN') {alert("Y-Dimension should be a number in \u03BCm!");} for (var i = 0; i < resY.length; i++) {resY[i] = yDim/files[i].height; yResDisp.value = resY[i].toFixed(2).toString().concat(" \u03BCm");}}); 

            distanceDisp = document.getElementById('distanceDisp'); 
            angleDisp = document.getElementById('angleDisp'); 
            
            //trigger deletion of selected rows
            document.getElementById("delete-rows").addEventListener("click", function(){
                table.deleteRow(table.getSelectedRows()); 
            });
            
            //trigger download of data.csv file
            document.getElementById("download-csv").addEventListener("click", function(){
                table.download("csv", "imagepicker.csv");
            });

            //trigger download of data.xlsx file
            document.getElementById("download-xlsx").addEventListener("click", function(){
                table.download("xlsx", "imagepicker.xlsx", {sheetName:"My Data"});
            });

            //trigger download of data.pdf file
            document.getElementById("download-pdf").addEventListener("click", function(){
                table.download("pdf", "imagepicker.pdf", {
                    orientation:"portrait", //set page orientation to portrait
                    title:"ImagePicker Data Export", //add title to report
                });
            });

            //trigger download of data.html file
            document.getElementById("download-html").addEventListener("click", function(){
                table.download("html", "imagepicker.html", {style:true});
            });
            

            //// HERE IS MY NEW CODE FOR RADIO BUTTON
            if (document.querySelector('input[name="pickwhat"]')) {
                document.querySelectorAll('input[name="pickwhat"]').forEach((elem) => {
                    elem.addEventListener("change", ev_tool_change, false);
                });
            }

            // Activate the default tool.
            if (tools[tool_default]) {
                tool = new tools[tool_default]();
            }

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);
            
            requestAnimationFrame(displayImage);
        }

        async function loopFiles(fileList, index) {
            if (index == fileList.length) {
                sliderF.max = files.length-1;
                setFrame(0);
            }
            else {
                const file = fileList[index]; 
                if (!file.type.startsWith('image/')){ 
                    alert('Error: The file you provided is not an image');
                    clearImages(); 
                    return;  }
                if (file.name.match(/.(tif|tiff)$/i)) {
                    // parse EXIF data with ExifReader
                    const tags = await ExifReader.load(file);
                    const ImageDescription = tags['ImageDescription'].description; 
                    var findResolutionX = ImageDescription.search("spatial-calibration-x"); 
                    var stringResolutionX = ImageDescription.substr(findResolutionX, 50); 
                    var findValue = stringResolutionX.search("value"); 
                    stringResolutionX = stringResolutionX.substr(findValue+8, 3);
                    const valueResolutionX = parseFloat(stringResolutionX);
                    var findResolutionY = ImageDescription.search("spatial-calibration-y"); 
                    var stringResolutionY = ImageDescription.substr(findResolutionY, 50); 
                    findValue = stringResolutionY.search("value"); 
                    stringResolutionY = stringResolutionY.substr(findValue+8, 3);
                    const valueResolutionY = parseFloat(stringResolutionY);
        	        fileNames[index] = file.name;
                    var reader = new FileReader(); 
                    reader.onload = ( function(){
                        return function(e) {
                            var buffer = e.target.result;
                            var tiff = new Tiff({buffer: buffer});
                            var canvas = tiff.toCanvas();
                            if (canvas) {
                                files[index] = canvas;
                                resX[index] = valueResolutionX; 
                                resY[index] = valueResolutionY; 
                                loopFiles(fileList, ++index); 
                            }
                        }
                    })(file);
                    reader.readAsArrayBuffer(file);
                }
                else {
                    const img = document.createElement("img");
                    img.classList.add("obj"); 
                    img.src = URL.createObjectURL(file);
  					fileNames[index] = file.name;
                    img.onload = function(){
                        files[index] = img;
                        resX[index] = 0.; 
                        resY[index] = 0.; 
                        loopFiles(fileList, ++index);
                    }
                }
            }

        }

        function handleFiles() {
            const fileList = this.files; 
            files = new Array();
            fileNames = new Array();
            resX = new Array(); 
            resY = new Array();
            loopFiles(fileList, 0); 
        }

        function clearImages() {
            for (let i = 0; i < files.length; i++) {
                URL.revokeObjectURL(files[i].src); 
            }
            files = new Array();
            fileNames = new Array();
            resX = new Array(); 
            resY = new Array();
            frame = -1;
            displayImage(); 
        }

        function setFrame(f)
        {
            if ( f != frame )
            {
                frame = f;
                valueF.innerHTML = frame.toString();
                xResDisp.value = resX[frame].toString().concat(" \u03BCm");
                yResDisp.value = resY[frame].toString().concat(" \u03BCm");
                xDimDisp.value = (resX[frame]*files[frame].width).toString().concat(" \u03BCm");
                yDimDisp.value = (resY[frame]*files[frame].height).toString().concat(" \u03BCm");
                requestAnimationFrame(displayImage);
            }
        }

        function setZoom(s) {
            if (s !== scale) {
                scale = s; 
                var zoom = (scale*100).toString().concat('%');
                valueZoom.innerHTML = zoom; 
                requestAnimationFrame(displayImage);
            }
        }

        // The general-purpose event handler. This function just determines the mouse 
        // position relative to the canvas element.
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        // The event handler for any changes made to the tool selector.
        function ev_tool_change () {
            if (tools[this.value]) {
                tool = new tools[this.value]();
            }
        }

        // This object holds the implementation of each drawing tool.
        var tools = {};

        // The drawing pencil.
        tools.pencil = function () {
            var tool = this;
            this.started = false;

            // This is called when you start holding down the mouse button.
            // This starts the pencil drawing.
            this.mousedown = function (ev) {
                context.beginPath();
                context.moveTo(ev._x, ev._y);
                tool.started = true;
            };

            // This function is called every time you move the mouse. Obviously, it only 
            // draws if the tool.started state is set to true (when you are holding down 
            // the mouse button).
            this.mousemove = function (ev) {
                if (tool.started) {
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                }
            };

            // This is called when you release the mouse button.
            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                }
            };
        };

        // The rectangle tool.
        tools.rect = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
                tool.started = true;
                tool.x0 = ev._x;
                tool.y0 = ev._y;
            };

            this.mousemove = function (ev) {
                if (!tool.started) {
                    return;
                }

                var x = Math.min(ev._x,  tool.x0),
                        y = Math.min(ev._y,  tool.y0),
                        w = Math.abs(ev._x - tool.x0),
                        h = Math.abs(ev._y - tool.y0);

                context.clearRect(0, 0, canvas.width, canvas.height);

                if (!w || !h) {
                    return;
                }

                context.strokeRect(x, y, w, h);
            };

            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                }
            };
        };

        // The point tool.
        tools.point = function () {
            var tool = this;
            this.started = false;

            // This is called when you start holding down the mouse button.
            // This starts the pencil drawing.
            this.mousedown = function (ev) {
                context.beginPath();
                context.moveTo(ev._x, ev._y);
                context.arc(ev._x,ev._y,1,0,2*Math.PI, false);
                context.fill();
                context.closePath(); 
                tool.started = true;
            };
        };

        // The distance tool.
        tools.distance = function () {
            var tool = this;
            this.started = false;

            // This is called when you start holding down the mouse button.
            // This starts the pencil drawing.
            this.mousedown = function (ev) {
                if (!tool.started) {
                    tool.x0 = ev._x;
                    tool.y0 = ev._y;
                    tool.x1 = ev._x;
                    tool.y1 = ev._y;
                    tool.started = true;
                } else {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.beginPath();
                    context.moveTo(tool.x0, tool.y0);
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                    context.closePath(); 
                    var dx, dy;
                    dx = (ev._x - tool.x0)/scale;
                    dy = (ev._y - tool.y0)/scale; 
                    if (resX[frame] > 0 && resY[frame] > 0) { 
                        dx *= resX[frame]; 
                        dy *= resY[frame];
                    }
                    var distance = Math.sqrt(dx*dx + dy*dy); 
                    if (resX[frame] > 0 && resY[frame] > 0) {
                        distance = distance.toFixed(2).toString().concat('\u03BCm')
                    }
                    else {
                        distance = distance.toFixed(2).toString().concat('px')

                    }
                    distanceDisp.value = distance; 
                    tool.started = false;
                }
            };

            this.mousemove = function (ev) {
                if (tool.started) {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.beginPath();
                    context.moveTo(tool.x0, tool.y0);
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                    context.closePath(); 
                    tool.x1 = ev._x;
                    tool.y1 = ev._y;
                }
            };
        };

        // The angle tool.
        tools.angle = function () {
            var tool = this;
            // This is called when you start holding down the mouse button.
            // This starts the pencil drawing.
            this.mousedown = function (ev) {
                if (!tool.started) {
                    tool.xf = ev._x;
                    tool.yf = ev._y;
                    tool.started = true;
                } else if (!tool.middle && tool.started) {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.beginPath();
                    context.moveTo(tool.xf, tool.yf);
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                    context.closePath(); 
                    tool.xm = ev._x; 
                    tool.ym = ev._y; 
                    tool.middle = true;
                }      
                else {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.beginPath();
                    context.moveTo(tool.xf, tool.yf);
                    context.lineTo(tool.xm, tool.ym);
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                    context.closePath(); 
                    tool.started = false;
                    tool.middle = false;
                    var ax = tool.xf - tool.xm; 
                    var ay = tool.yf - tool.ym; 
                    var bx = ev._x - tool.xm; 
                    var by = ev._y - tool.ym; 
                    var anorm = Math.sqrt(ax*ax+ay*ay);
                    var bnorm = Math.sqrt(bx*bx+by*by);
                    var angle = 180*Math.acos((ax*bx+ay*by)/(anorm*bnorm))/Math.PI; 
                    angle = angle.toFixed(2);
                    angleDisp.value = angle;
                    
                    var posx = tool.xm/scale; 
                    var posy = tool.ym/scale; 
                    if (resX[frame] > 0 && resY[frame] > 0) { 
                        posx *= resX[frame]; 
                        posy *= resY[frame];
                    }
                    posx = posx.toFixed(2); 
                    posy = posy.toFixed(2); 
                    if (files.length > 0) table.addData([{frame:fileNames[frame].toString(), posx:posx.toString(), posy:posy.toString(), angle:angle.toString(), outcome:"undefined"}], false);
					else table.addData([{frame:"no file", posx:posx.toString(), posy:posy.toString(), angle:angle.toString(), outcome:"undefined"}], false);
                }
            };

            this.mousemove = function (ev) {
                if (tool.started) {

                    context.clearRect(0, 0, canvas.width, canvas.height);

                    context.beginPath();
                    context.moveTo(tool.xf, tool.yf);
                    if (tool.middle) {
                        context.lineTo(tool.xm, tool.ym);
                    }
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                    context.closePath(); 
                }
            };
        };

        function displayImage()
        {
            if (files.length > 0) {
                var image = files[frame]; 
                if ((canvas.width !== scale*image.width) || (canvas.height !== scale*image.height)) {
                    canvas.width = scale*image.width;
                    canvas.height = scale*image.height; 
                    imageCanvas.width = scale*image.width;
                    imageCanvas.height = scale*image.height; 
                    var widthstr = (scale*image.width).toString().concat('px');
                    var heightstr = (scale*image.height).toString().concat('px');
                    canvasWrapper.style.width = widthstr;
                    canvasWrapper.style.height = heightstr; 
                }
                imageContext.clearRect(0, 0, imageCanvas.width, imageCanvas.height);
                imageContext.setTransform(1, 0, 0, 1, 0, 0);
                imageContext.scale(scale, scale);
                imageContext.drawImage(image, 0, 0);
                drawGrid();
            }
            else {
                imageContext.clearRect(0, 0, imageCanvas.width, imageCanvas.height);
                drawGrid();
            }
            updateColor();

        }

        function changeColor() {
            color = this.value;
            updateColor(); 
        }

        function updateColor() {
            context.strokeStyle = color; 
            context.fillStyle = color; 
        }

		function drawGrid() {
			if (grid) {
				imageContext.beginPath();
				imageContext.lineWidth = 0.5;
				for (var x = gridSize; x < canvas.width; x += gridSize) {
	        		imageContext.moveTo(x, 0);
	        		imageContext.lineTo(x, canvas.height );
	    		}
	    		for (var x = gridSize; x < canvas.height; x += gridSize) {
	        		imageContext.moveTo(0, x );
	        		imageContext.lineTo(canvas.width, x);
	   			}
	    		imageContext.strokeStyle = gridColor;
	    		imageContext.stroke();
	    		imageContext.closePath();
	    	}
		}
        init();

    }, false); }
